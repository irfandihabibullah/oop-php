<?php
require_once'animal.php';
$sheep = new Animal("shaun",2,"false");
echo "<br><br>";
$sungokong = new Ape("kera sakti");
$sungokong->yell() ;// "Auooo"
echo "<br><br>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>
